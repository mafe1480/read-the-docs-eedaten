=========================================
Renewable Energy Sources (RES) in Germany
=========================================

Firstly, power plants can be classified in terms of energy sources.
Technically, each energy source displays a table where data of the same category is stored.
Following sources can be distinguished:

  * solar energy
  * wind energy
  * biomass
  * hydro power
  * storage (including pump storage)
  * geothermal and solar thermal energy
  * landfill, sewer and mine gas

Furthermore, MaStR contains conventional power plants like lignite, hard coal, natural gas, nuclear power, etc. as well.
All following analysis deal with RES what underlines the focus of EEDATEN.


.. toctree::
   :maxdepth: 2

   solar
   wind
   biomass
