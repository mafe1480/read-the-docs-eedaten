.. _sec-ml:

==================================
Machine Learning (ML) and RES data
==================================

Machine learning is referred to the following problem formulation according to `scikit-learn <https://scikit-learn.org/stable/tutorial/basic/tutorial.html>`__, a free software machine learning library for Python that will be used for upcoming ML problems.
Learning process takes n samples of data (with several features) as input and predicts properties of unknown data.

For a better overview, the learning process can be distinguished between supervised and unsupervised learning.
It should be noted that more learning fields exist for example reinforcement learning, but will not be considered further here.

.. image:: picture/ml.jpg
  :width: 300px
  :height: 300px
  :align: center


.. toctree::
   :maxdepth: 2

   predict_vl_wind
