.. _sec-data-source:

===========
Data source
===========

The `Marktstammdatenregister <https://www.marktstammdatenregister.de/MaStR/>`__ (MaStR) as a public state register collects master data of all power plants in Germany.
It contains data of new and existing plants (renewable, conventional) which produce electricity or gas.
The question which plants have to be registered is relatively simple to be answered: **all plants that produce electricity**

As mentioned earlier in this documentation, difficulties may arise when dealing with data of renewable energy sources.
Plant operators make entries in the system on their own, keep data up to date and take care about data quality.
Unfortunately errors occur in the register process whereby validity suffers due to bad or missing data.
For this reason grid operators are included into the whole process by accessing the register through an API and verify entered data.
The verification process to keep data clean seems time consuming and the amount of data amplifies this assumption.
In the end the requested raw data still contain unverified and wrong data what makes data preprocessing necessary.
Most common errors are:

    * wrong installed power (e.g. mixed up units kW, MW, thousands separator)
    * missing voltage level
    * no indication of location
    * wrong location (e.g. territorial reform)

EEDATEN implements test functions to verify wrong locations and indicates false information regarding installed power.
Both functions are an integral part of data preprocessing.
Based on this, further calculations take cleaned data as input to perform the mapping or predict missing informations like voltage level using machine learning (ML) algorithms.

The data provided by MaStR are licensed under `Data licence Germany – attribution – version 2.0 <https://www.govdata.de/dl-de/by-2-0>`__, `legal notices MaStR <https://www.marktstammdatenregister.de/MaStR/Startseite/Impressum>`__

Again, it should be noted that MaStR has not yet published all master data of power plants thus performed calculations and analysis are only valid for limited number of plants.
Further updates will keep the data up to date.
Current state of requested power plants can be found on `EEDATEN's website <https://www.eedaten.de>`__.


.. toctree::
   :maxdepth: 2

   map_data
