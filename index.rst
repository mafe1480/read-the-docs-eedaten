.. eedaten documentation master file, created by
   sphinx-quickstart on Fri Nov 13 20:01:16 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

###################################
Welcome to EEDATEN's documentation!
###################################

.. image:: picture/ee_historisch.jpg
  :width: 350px
  :height: 320px
  :align: right

With regard to Germany, EEDATEN maps renewable energy sources and their data with spatial data.
This approach leads to a distributed representation of renewable energy sources in Germany and addresses the following issues when dealing with data in context of renewable energies and their integration in the electric power system:

  * data availability
  * data volume
  * data quality (wrong or missing data)
  * individual vs. aggregated data (e.g. power per plant, sum of installed power)
  * generate knowledge from data (data, information, knowledge)

The following sections document data, data structure, data preprocessing and analysis as well as visualizations that were implemented on EEDATEN.

To access EEDATEN, please use the following link: `EEDATEN's website <https://www.eedaten.de>`__

It is important to note that EEDATEN has been created and implemented as an private project.
Because the used data source has not yet fully published all data, further data updates will be necessary to display the current status of renewables in Germany (last updated: December 2020).

For more information, suggestions, ideas, please contact me via kontakt@eedaten.de

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   system_architecture
   data_source
   preprocessing
   res
   analysis
