*************************************************
Predict voltage levels (VL) for wind power plants
*************************************************

.. image:: picture/clf.png
  :width: 300px
  :height: 300px
  :align: center

.. image:: picture/wind_se.png
  :width: 300px
  :height: 300px
  :align: center
